#[cfg(not(target_os = "linux"))]
mod dummy;
#[cfg(target_os = "linux")]
mod gpio;

use env_logger::Env;
use log::{error, info};
use std::error::Error;
use std::ops::ControlFlow;
use std::process::Command;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::time::{Duration, Instant};
#[cfg(target_os = "linux")]
use systemd_journal_logger::{connected_to_journal, init_with_extra_fields};

fn init_env_logger() {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();
}

#[cfg(target_os = "linux")]
fn init_logging() {
    if connected_to_journal() {
        init_with_extra_fields(vec![("VERSION", env!("CARGO_PKG_VERSION"))]).unwrap();
        log::set_max_level(log::LevelFilter::Info);
    } else {
        init_env_logger()
    }
}

#[cfg(not(target_os = "linux"))]
fn init_logging() {
    init_env_logger()
}

#[cfg(not(target_os = "linux"))]
fn init_input() -> Result<impl Input, Box<dyn Error>> {
    dummy::DummyInput::new()
}

#[cfg(target_os = "linux")]
fn init_input() -> Result<impl Input, Box<dyn Error>> {
    gpio::GpioInput::new()
}

trait Input {
    fn is_pressed(&self) -> bool;
}

fn run_shutdown_command() -> Result<(), Box<dyn Error>> {
    let output = Command::new("poweroff").output()?;

    if !output.status.success() {
        let stdout = String::from_utf8(output.stdout)?;
        let stderr = String::from_utf8(output.stderr)?;
        error!(
            "Trigger program failed, stdout: {} | stderr: {}",
            stdout, stderr
        );
    }

    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    init_logging();

    info!("shutdownd initiated, press the button for 2 seconds");

    let shutdown = Arc::new(AtomicBool::new(false));
    let input = init_input()?;

    signal_hook::flag::register(signal_hook::consts::SIGTERM, Arc::clone(&shutdown))?;
    signal_hook::flag::register(signal_hook::consts::SIGINT, Arc::clone(&shutdown))?;

    let result = (0..)
        .into_iter()
        .try_fold(None, |start: Option<Instant>, _| {
            if shutdown.load(Ordering::Relaxed) {
                ControlFlow::Break(Ok(()))
            } else {
                std::thread::sleep(Duration::from_millis(100));

                if input.is_pressed() {
                    let start = start.unwrap_or_else(Instant::now);
                    if start.elapsed().as_secs() > 2 {
                        info!("Shutdown triggered, goodbye...");

                        ControlFlow::Break(run_shutdown_command())
                    } else {
                        ControlFlow::Continue(Some(start))
                    }
                } else {
                    ControlFlow::Continue(None)
                }
            }
        });

    if let ControlFlow::Break(Err(e)) = result {
        Err(e)
    } else {
        info!("Exiting, goodbye");

        Ok(())
    }
}
