use crate::Input;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

pub struct DummyInput {
    trigger: Arc<AtomicBool>,
}

impl DummyInput {
    pub fn new() -> Result<Self, Box<dyn std::error::Error>> {
        let d = DummyInput {
            trigger: Default::default(),
        };
        signal_hook::flag::register(signal_hook::consts::SIGQUIT, Arc::clone(&d.trigger))?;

        Ok(d)
    }
}

impl Input for DummyInput {
    fn is_pressed(&self) -> bool {
        match self
            .trigger
            .compare_exchange(true, false, Ordering::Relaxed, Ordering::Relaxed)
        {
            Ok(true) => true,
            Err(false) => false,
            _ => unreachable!(),
        }
    }
}
