use crate::Input;
use gpio_cdev::Chip;
use gpio_cdev::LineHandle;
use log::warn;

pub struct GpioInput {
    handle: LineHandle,
}

impl GpioInput {
    pub fn new() -> Result<Self, Box<dyn std::error::Error>> {
        let mut chip = Chip::new("/dev/gpiochip0")?;
        let handle =
            chip.get_line(3)?
                .request(gpio_cdev::LineRequestFlags::INPUT, 0, "read-input")?;

        Ok(GpioInput { handle })
    }
}

impl Input for GpioInput {
    fn is_pressed(&self) -> bool {
        match self.handle.get_value() {
            Ok(0) => true,
            Ok(1) => false,
            Err(e) => {
                warn!("Could not read GPIO-pin: {}", e);
                false
            }
            _ => unreachable!(),
        }
    }
}
